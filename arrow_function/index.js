function sayHelo() {
  console.log("Hello");
}

let say_hello = () => {
  console.log("hello");
};
say_hello();

let say_hello_by_name = (name) => {
  console.log("Hello", name);
};
say_hello_by_name("Alice Nguyennnnnnnn");

//  npm install -g nodemon
// bind call apply

// map
// forEach

let doubleNumber = (number) => number * 2;

let value1 = doubleNumber(20);
console.log("😀 - value1", value1);
