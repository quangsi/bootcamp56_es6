// pass by reference: array, object
// pass by value : string,number, boolean

// deep copy, shallow copy
let cat1 = {
  name: "meo meo",
  rating: " 5 starts",
};

// let cat2 = { ...cat1 };
// cat2.rating = "1 start";
let cat2 = { ...cat1, rating: "3 starts", color: "Black" };

console.log("😀 - cat1", cat1);
console.log("😀 - cat2", cat2);

// array
let colors = ["red", "blue"];
// newColors.push("green");
let newColors = [...colors, "green", "pink"];
console.log("😀 - colors", colors);
console.log("😀 - newColors", newColors);
