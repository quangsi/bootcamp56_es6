// let tinhTong = (n1, n2) => {
//   return n1 + n2;
// };
let tinhTong = (n1, n2 = 20) => n1 + n2;

let ketQua1 = tinhTong(20);
console.log("😀 - ketQua1", ketQua1);
let ketQua2 = tinhTong(2, 3);
console.log("😀 - ketQua2", ketQua2);
