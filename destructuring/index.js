let user = {
  name: "alice",
  age: 20,
  child: {
    name: "alice baby",
    age: 1,
  },
};
// detructuring and rename
let { name: babyName, age: babyAge } = user.child;
console.log("😀 - babyName", babyName);
let { name, age } = user;
console.log("😀 - name, age", name, age);
