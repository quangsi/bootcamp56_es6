let colors = ["red", "blue", "green"];

// forEach : duyệt mảng
colors.forEach(function (item, index) {
  console.log("😀 - index", index);
  console.log("😀 - item", item);
});

// callback

// map ~ convert data ~ return
let colorsHTML = colors.map(function (item, index) {
  return `<span>${item}</span>`;
});
console.log("😀 - colorsHTML - colorsHTML", colorsHTML);
