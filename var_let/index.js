// hoisting
// var username;
// console.log("😀 - username", username);
// username = "alice";
// username = "bob";
console.log("😀 - username", username);
var username = "alice";
var username = "bob";
let account = "alice@gmail.com";
console.log("😀 - account", account);
// var =>cho phép khai báo trùng tên
// let => ko cho phép
// scope ~ phạm vi hoạt động
// var => function scope => bị giới hạn bởi function
function checkLogin() {
  var isLogin = true;
  if (isLogin) {
    var message = "Success";
  } else {
    var message = "Fail";
  }
  console.log("😀 - checkLogin - message", message);
}
checkLogin();
// console.log("😀 - checkLogin - isLogin", isLogin);
function check_login() {
  let is_login = true;
  if (is_login) {
    let message = "Success";
  } else {
    let message = "Fail";
  }
  console.log("😀 - check_login - message", message);
}
check_login();
// let => block scope
// lỗi 1, ko lỗi 0
